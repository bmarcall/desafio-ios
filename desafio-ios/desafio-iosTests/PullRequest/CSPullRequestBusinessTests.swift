//
//  CSPullRequestBusinessTests.swift
//  desafio-iosTests
//
//  Created by Marçal on 13/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import XCTest
import Foundation
@testable import desafio_ios

class CSPullRequestBusinessTests: desafio_iosTests {
	
	// MARK: - Constants
	
	fileprivate let business = CSPullRequestBusiness()
	fileprivate let Login = "Alamofire"
	fileprivate let repositoryName = "Alamofire"

	func testPullRequestValidateSucceed() {
		let provider = CSPullRequestApiProviderMock(statusCode: .Ok, file: "pullRequest")
		business.provider = provider
		
		let expectation = self.expectation(description: "Wait for pullRequest data")
		
		business.getPullRequest(login: Login, repositoryName: repositoryName, successBlock: { (pull) in
			XCTAssertNotNil(pull.pullRequest)
			expectation.fulfill()
		}) { (error) in }
		
		self.waitForExpectations(timeout: 3, handler: nil)
	}

	func testPullRequestValidateFailed() {
		let provider = CSPullRequestApiProviderMock(statusCode: .Unknown, file: "pullRequest-file-not-exist")
		business.provider = provider
		
		let expectation = self.expectation(description: "Wait for pullRequest failed")

		business.getPullRequest(login: Login, repositoryName: repositoryName, successBlock: { (pull) in
		}) { (error) in
			expectation.fulfill()
		}

		self.waitForExpectations(timeout: 3, handler: nil)
	}

}
