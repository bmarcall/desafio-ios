//
//  CSBaseProviderMock.swift
//  desafio-iosTests
//
//  Created by Marçal on 13/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import Foundation
@testable import desafio_ios

public struct DataResponse {
	
	public let response: String?
	public let statusCode: CSHttpStatusCode
	public var value: AnyObject?
	public var error: NSError?
	
	public init(statusCode: CSHttpStatusCode, response: String?, value: AnyObject?, error: NSError?) {
		self.statusCode = statusCode
		self.response = response
		self.value = value
		self.error = error
	}
}

class CSBaseProviderMock {
	
	fileprivate var statusCode: CSHttpStatusCode
	fileprivate var readData: Bool
	fileprivate var jsonFile: String
	fileprivate var hasError: Bool
	fileprivate var jsonFiles: [String]?
	
	init(statusCode: CSHttpStatusCode, file: String = String(), hasError: Bool = false, jsonFiles: [String]? = nil) {
		self.statusCode = statusCode
		jsonFile = file
		self.jsonFiles = jsonFiles
		readData = file != String() || jsonFiles != nil
		self.hasError = hasError
	}
	
	func loadResponse(fileIndex: Int? = nil) -> DataResponse? {
		let file = fileIndex ?? nil
		var dataResponse: DataResponse!
		var error: NSError?
		if (hasError || self.statusCode != .Ok) {
			error = NSError(domain: "erro de teste", code: self.statusCode.rawValue, userInfo: nil)
		}
		
		if readData {
			if let jsonResponse = jsonWithFile(fileIndex: file) {
				dataResponse = DataResponse.init(statusCode: self.statusCode, response: self.jsonFile, value: jsonResponse as AnyObject, error: error)
			} else {
				dataResponse = DataResponse.init(statusCode: self.statusCode, response: self.jsonFile, value: nil, error: NSError(domain: "json não encontrado nos arquivos", code: self.statusCode.rawValue, userInfo: nil))
			}
		}
		return dataResponse
	}
	
	func jsonWithFile(fileIndex: Int?) -> [String: Any]? {
		var filename = jsonFile
		if let index = fileIndex, let files = jsonFiles {
			filename = files[index]
		}
		
		if let path = Bundle(for: type(of: self)).path(forResource: filename, ofType: "json") {
			if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
				do {
					if let dictionary = try (JSONSerialization.jsonObject (with: data, options: .allowFragments) as? [String: Any]) {
						return dictionary
					}
				} catch {
					return nil
				}
			}
		}
		
		return nil
	}
}
