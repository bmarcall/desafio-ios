//
//  CSRepositoryBusinessTests.swift
//  desafio-iosTests
//
//  Created by Marçal on 13/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import XCTest
import Foundation
@testable import desafio_ios


class CSRepositoryBusinessTests: desafio_iosTests {
	
	// MARK: - Constants
	
	fileprivate let business = CSRepositoryBusiness()
	fileprivate let Language = "Swift"
	fileprivate let Sort = "stars"
	fileprivate let currentPage = "1"
	
	func testRepositoryValidateSucceed() {
		let provider = CSRepositoryApiProviderMock(statusCode: .Ok, file: "repository")
		business.provider = provider
		
		let expectation = self.expectation(description: "Wait for repository data")
		
		business.getRepository(language: Language, sort: Sort, pageCount: currentPage, successBlock: { (repo) in
			XCTAssertNotNil(repo.repository)
			expectation.fulfill()
		}) { (error) in }
		
		self.waitForExpectations(timeout: 3, handler: nil)
	}
	
	func testRepositoryValidateFailed() {
		let provider = CSRepositoryApiProviderMock(statusCode: .Unknown, file: "repository-file-not-exist")
		business.provider = provider
		
		let expectation = self.expectation(description: "Wait for repository failed")
		
		business.getRepository(language: Language, sort: Sort, pageCount: currentPage, successBlock: { (repo) in
		}) { (error) in
			expectation.fulfill()
		}
		
		self.waitForExpectations(timeout: 3, handler: nil)
	}
}
