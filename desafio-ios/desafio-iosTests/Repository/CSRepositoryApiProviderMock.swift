//
//  CSRepositoryProviderMock.swift
//  desafio-iosTests
//
//  Created by Marçal on 13/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import XCTest
import Foundation
@testable import desafio_ios

class CSRepositoryApiProviderMock: CSBaseProviderMock, CSRepositoryApiProtocol {

	func getRepository(language: String, sort: String, pageCount: String, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback) {
		self.handleResponse(successBlock: successBlock, failureBlock: failureBlock)
	}
	
	func handleResponse(successBlock: CSSuccessCallback, failureBlock: CSFailureCallback) {
		let response = self.loadResponse()
		if ((response?.error) != nil) {
			failureBlock((response?.error)!)
			return
		}
		if let data = response?.value {
			successBlock(data)
		} else {
			failureBlock(CSProviderError.GenericError)
		}
	}
}

