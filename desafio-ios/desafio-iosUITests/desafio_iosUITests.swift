//
//  desafio_iosUITests.swift
//  desafio-iosUITests
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import XCTest

class desafio_iosUITests: XCTestCase {
        
	var app: XCUIApplication!
	
	override func setUp() {
		super.setUp()
		continueAfterFailure = false
		app = XCUIApplication()
		app.launch()
		
	}
	
	func testRepositoryPullRequestScreen() {
		let tablesQuery = app.tables
		tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["repoID: 22458259"]/*[[".cells[\"Optional(22458259)\"].staticTexts[\"repoID: 22458259\"]",".staticTexts[\"repoID: 22458259\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
		app.navigationBars["Alamofire"].buttons["Swift Repos"].tap()
		tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["repoID: 20965586"]/*[[".cells[\"Optional(20965586)\"].staticTexts[\"repoID: 20965586\"]",".staticTexts[\"repoID: 20965586\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
		app.navigationBars["SwiftyJSON"].buttons["Swift Repos"].tap()
	}
}
