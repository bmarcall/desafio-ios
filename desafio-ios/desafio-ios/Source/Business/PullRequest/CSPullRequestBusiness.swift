//
//  CSPullRequestBusiness.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

typealias CSSuccessPullRequestDataCallback = (_ pullData: CSPullRequestData) -> Swift.Void

class CSPullRequestBusiness: CSBaseBusiness {
	
	// MARK: - Constants
	
	fileprivate let kPullsLabel = "pulls"
	
	// MARK: - Properties
	
	var provider: CSPullRequestApiProtocol!
	
	// MARK: - Initializers
	
	override init() {
		self.provider = CSPullRequestApiProvider()
	}
	
	init(provider: CSPullRequestApiProvider) {
		self.provider = provider
	}

	func getPullRequest(login: String, repositoryName: String, successBlock: @escaping CSSuccessPullRequestDataCallback, failureBlock: @escaping CSFailureCallback) {
		self.provider.getPullRequest(login: login, repositoryName: repositoryName, successBlock: { (data) in
			var pullsDictionary: [String: AnyObject] = [:]
			if data is Array<Any> {
				pullsDictionary =  [self.kPullsLabel: data]
			} else {
				pullsDictionary = data as! [String: AnyObject]
			}
			
			if let result = CSPullRequestData.parse(dictionary: pullsDictionary) {
				successBlock(result)
			}
		}) { (error) in
			failureBlock(error)
		}
	}
}


