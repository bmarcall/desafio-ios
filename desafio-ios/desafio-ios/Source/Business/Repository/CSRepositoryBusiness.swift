//
//  CSRepositoryBusiness.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

typealias CSSuccessRepositoryDataCallback = (_ repo: CSRepositoryData) -> Swift.Void

class CSRepositoryBusiness: CSBaseBusiness {
	
	// MARK: - Properties
	
	var provider: CSRepositoryApiProtocol!
	
	// MARK: - Initializers
	
	override init() {
		self.provider = CSRepositoryApiProvider()
	}
	
	init(provider: CSRepositoryApiProvider) {
		self.provider = provider
	}
	
	func getRepository(language: String, sort: String, pageCount: String, successBlock: @escaping CSSuccessRepositoryDataCallback, failureBlock: @escaping CSFailureCallback) {
		self.provider.getRepository(language: language, sort: sort, pageCount: pageCount, successBlock: { (data) in
			let repoDictionary = data as! [String: AnyObject]
			if let result = CSRepositoryData.parse(dictionary: repoDictionary) {
				successBlock(result)
			}
		}) { (error) in
			failureBlock(error)
		}
	}
}

