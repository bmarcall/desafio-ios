//
//  CSPullRequestTableViewCell.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit
import SDWebImage
import TSMarkdownParser

class CSPullRequestTableViewCell: UITableViewCell {

	fileprivate let kUserImage = "profile"
	
	@IBOutlet var imgProfile: UIImageView!
	@IBOutlet var txtTitleLabel: UILabel!
	@IBOutlet var txtAuthorLabel: UILabel!
	@IBOutlet var txtDateLabel: UILabel!
	@IBOutlet var txtBodyLabel: UILabel!
	
	
	lazy var dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.calendar = Calendar(identifier: .iso8601)
		formatter.locale = Locale(identifier: "en_US_POSIX")
		formatter.timeZone = TimeZone(secondsFromGMT: 0)
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
		return formatter
	}()

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)

		// Configure the view for the selected state
	}

	func setup(pull: CSPullRequest) {
		imgProfile.layer.masksToBounds = true
		imgProfile.layer.cornerRadius = imgProfile.frame.width / 2
		imgProfile.sd_setImage(with: URL(string: (pull.User?.AvatarURL)!), placeholderImage: UIImage(named: kUserImage), options: SDWebImageOptions.highPriority, completed: nil)
		
		txtTitleLabel.text = pull.Title
		txtAuthorLabel.text = pull.User?.Login
		
		if let date = dateFormatter.date(from: pull.CreateDate!){
			dateFormatter.dateStyle = .long
			dateFormatter.timeStyle = .none

			self.txtDateLabel.text = dateFormatter.string(from: date)
		}

		guard
			let markdown = pull.Body
		else {
			self.txtBodyLabel.text = pull.Body
			return
		}

		self.txtBodyLabel.attributedText = TSMarkdownParser.standard().attributedString(fromMarkdown: markdown)
	}
}
