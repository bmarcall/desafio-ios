//
//  CSPullRequestViewController.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSPullRequestViewController: UIViewController {

	fileprivate let kCellIdentifier = "pullCell"
	fileprivate let kTableviewHeight = CGFloat(133)
	fileprivate let kValue0 = 0

	@IBOutlet var tView: UITableView!
	
	private let manager = CSPullRequestManager()
	private var pull = [CSPullRequest]()
	
	var Login = ""
	var Repositoryname = ""
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.customizeControls()
		self.getPullRequest()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Methods

	func customizeControls() {
		self.title = Repositoryname

		tView.estimatedRowHeight = 80.0
		tView.rowHeight = UITableViewAutomaticDimension
	}
	
	func getPullRequest() {
		self.manager.getPullRequest(login: Login, repositoryName: Repositoryname, successBlock: { (pullRequest) in
			if let count = pullRequest.pullRequest?.count {
				if count > 0 {
					self.pull = pullRequest.pullRequest!
					self.tView.reloadData()
				}
			}
		}) { (error) in
			print("error: ", error)
		}
	}
}

extension CSPullRequestViewController: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let count = self.pull.count
		if count > 0 {
			return count
		} else {
			return kValue0
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath as IndexPath) as! CSPullRequestTableViewCell
		
		cell.setup(pull: self.pull[indexPath.row])
		
		return cell
	}
}
