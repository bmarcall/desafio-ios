//
//  CSRepositoryViewController.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSRepositoryViewController: UIViewController {

	// MARK: - Constants
	
	fileprivate let kCellIdentifier = "repoCell"
	fileprivate let kTableviewHeight = CGFloat(123)
	fileprivate let kValue0 = Int(0)
	fileprivate let kLanguage = "Swift"
	fileprivate let kSort = "stars"
	fileprivate let kPullRequestViewController = "CSPullRequestViewController"
	fileprivate let kPullURLComp = "/pulls"

	fileprivate let DISTANCE_TO_BOTTON: CGFloat = -40
	
	@IBOutlet var tView: UITableView!
	
	private let manager = CSRepositoryManager()
	private var repos = [CSRepository]()
	private var currentPage = 1
	
	override func viewDidLoad() {
		super.viewDidLoad()

		tView.estimatedRowHeight = 123.0
		tView.rowHeight = UITableViewAutomaticDimension

		self.getRepository(page: String(describing: currentPage))
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	func getRepository(page: String) {
		self.manager.getRepository(language: kLanguage, sort: kSort, pageCount: page, successBlock: { (repoData) in
			if let count = repoData.repository?.count {
				if count > 0 {
					self.repos += repoData.repository!
					self.currentPage += 1
					self.tView.reloadData()
				}
			}
		}) { (error) in
			print("error: ", error)
		}
	}
}

extension CSRepositoryViewController: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return kTableviewHeight
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let count = self.repos.count
		if count > 0 {
			return count
		} else {
			return kValue0
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath as IndexPath) as! CSRepositoryTableViewCell

		cell.accessibilityIdentifier = String(describing: self.repos[indexPath.row].repoID)
		cell.setup(repository: self.repos[indexPath.row])
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let viewController = self.storyboard?.instantiateViewController(withIdentifier: kPullRequestViewController) as! CSPullRequestViewController
		viewController.Login = (self.repos[indexPath.row].owner?.login)!
		viewController.Repositoryname = (self.repos[indexPath.row].repoName)!
		self.navigationController?.pushViewController(viewController, animated: true)
	}
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		
		let currentYPosition = targetContentOffset.pointee.y
		let maximumOffset = scrollView.contentSize.height - scrollView.bounds.height
		
		if currentYPosition - maximumOffset >= DISTANCE_TO_BOTTON {
			self.getRepository(page: String(describing: currentPage))
		}
	}
}
