//
//  CSRepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit
import SDWebImage

class CSRepositoryTableViewCell: UITableViewCell {

	// MARK: - Constants
	
	fileprivate let kUserImage = "user"
	fileprivate let kRepoIDLabel = "repoID: "
	
	@IBOutlet var imgUser: UIImageView!
	@IBOutlet var txtRepoName: UILabel!
	@IBOutlet var txtDesc: UILabel!
	@IBOutlet var txtStarLabel: UILabel!
	@IBOutlet var txtForkLabel: UILabel!
	@IBOutlet var txtRepoID: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)

		// Configure the view for the selected state
	}
	
	// MARK: - Methods
	
	func setup(repository: CSRepository) {
		imgUser.layer.masksToBounds = true
		imgUser.layer.cornerRadius = imgUser.frame.size.width / 2
		imgUser.sd_setImage(with: URL(string: (repository.owner?.avatarURL)!), placeholderImage: UIImage(named: kUserImage), options: SDWebImageOptions.highPriority, completed: nil)
		
		txtRepoID.text = kRepoIDLabel + String(describing: repository.repoID!)
		txtRepoName.text = repository.fullName
		txtDesc.text = repository.desc
		txtStarLabel.text = String(describing: repository.starsCount!)
		txtForkLabel.text = String(describing: repository.forksCount!)
	}

}
