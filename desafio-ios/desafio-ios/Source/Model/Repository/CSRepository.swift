//
//  CSRepository.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSRepository: NSObject, CSBaseModelProtocol {

	// MARK: - Constants
	
	fileprivate static let kRepoID = "id"
	fileprivate static let kRepoName = "name"
	fileprivate static let kFullName = "full_name"
	fileprivate static let kDesc = "description"
	fileprivate static let kOwner = "owner"
	fileprivate static let kStarsCount = "stargazers_count"
	fileprivate static let kForksCount = "forks_count"
	fileprivate static let kPullsURL = "url"
	
	// MARK: - Properties
	
	var repoID: Int?
	var repoName: String?
	var fullName: String?
	var desc: String?
	var owner: CSOwner?
	var starsCount: Int?
	var forksCount: Int?
	var PullsURL: String?
	
	override init() { }

	init(repoID: Int?, repoName: String?, fullName: String?, desc: String?, owner: CSOwner?, starsCount: Int?, forksCount: Int?, pullsURL: String?) {
		self.repoID = repoID
		self.repoName = repoName
		self.fullName = fullName
		self.desc = desc
		self.owner = owner
		self.starsCount = starsCount
		self.forksCount = forksCount
		self.PullsURL = pullsURL
	}
	
	static func parse(dictionary: [String: AnyObject]) -> CSRepository? {
		var repo = CSRepository()

		fillWithDictionary(&repo.repoID, key: kRepoID, dictionary: dictionary)
		fillWithDictionary(&repo.repoName, key: kRepoName, dictionary: dictionary)
		fillWithDictionary(&repo.fullName, key: kFullName, dictionary: dictionary)
		fillWithDictionary(&repo.desc, key: kDesc, dictionary: dictionary)
		fillWithDictionary(&repo.starsCount, key: kStarsCount, dictionary: dictionary)
		fillWithDictionary(&repo.forksCount, key: kForksCount, dictionary: dictionary)
		fillWithDictionary(&repo.PullsURL, key: kPullsURL, dictionary: dictionary)

		guard
			let ownerDictionary = dictionary[kOwner] as? [String: AnyObject]
		else {
			return nil
		}
		
		let owner = CSOwner.parse(dictionary: ownerDictionary)
		repo = CSRepository(repoID: repo.repoID, repoName: repo.repoName, fullName: repo.fullName, desc: repo.desc, owner: owner, starsCount: repo.starsCount, forksCount: repo.forksCount, pullsURL: repo.PullsURL)
		
		return repo
	}
}
