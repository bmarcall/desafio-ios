//
//  CSRespositoryData.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSRepositoryData: NSObject, CSBaseModelProtocol {
	
	// MARK: - Constants
	
	fileprivate static let kItem = "items"
	
	// MARK: - Properties
	
	var repository: [CSRepository]?
	
	override init() { }
	
	init(repository: [CSRepository]?) {
		self.repository = repository
	}
	
	static func parse(dictionary: [String: AnyObject]) -> CSRepositoryData? {
		let repoData = CSRepositoryData()
		
		if let repoDataDictionary = dictionary[kItem] as? [[String : AnyObject]] {
			var repoDataArray = [CSRepository]()
			for item in repoDataDictionary {
				if let repoDataContent = CSRepository.parse(dictionary: item) {
					repoDataArray.append(repoDataContent)
				}
			}
			
			repoData.repository = repoDataArray
		}
		
		return repoData
	}
	
	
}


