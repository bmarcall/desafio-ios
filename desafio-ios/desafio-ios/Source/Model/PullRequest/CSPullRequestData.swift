//
//  CSPullRequestData.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSPullRequestData: NSObject, CSBaseModelProtocol {
	
	// MARK: - Constants
	
	fileprivate static let kPulls = "pulls"
	
	// MARK: - Properties
	
	var pullRequest: [CSPullRequest]?
	
	override init() { }
	
	init(pullRequest: [CSPullRequest]?) {
		self.pullRequest = pullRequest
	}
	
	static func parse(dictionary: [String: AnyObject]) -> CSPullRequestData? {
		let pullData = CSPullRequestData()
		
		if let pullDataDictionary = dictionary[kPulls] as? [[String : AnyObject]] {
			var pullDataArray = [CSPullRequest]()
			for item in pullDataDictionary {
				if let pullDataContent = CSPullRequest.parse(dictionary: item) {
					pullDataArray.append(pullDataContent)
				}
			}
			
			pullData.pullRequest = pullDataArray
		}
		
		return pullData
	}
}
