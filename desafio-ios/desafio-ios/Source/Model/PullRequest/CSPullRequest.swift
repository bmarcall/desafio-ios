//
//  CSPullRequest.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSPullRequest: NSObject, CSBaseModelProtocol {
	
	// MARK: - Constants
	
	fileprivate static let kTitle = "title"
	fileprivate static let kUser = "user"
	fileprivate static let kBody = "body"
	fileprivate static let kCreateDate = "created_at"
	fileprivate static let kUpdateDate = "updated_at"
	
	// MARK: - Properties
	
	var Title: String?
	var User: CSUser?
	var Body: String?
	var CreateDate: String?
	var UpdateDate: String?
	
	override init() { }
	
	init(title: String?, user: CSUser?, body: String?, createDate: String?, updateDate: String?) {
		self.Title = title
		self.User = user
		self.Body = body
		self.CreateDate = createDate
		self.UpdateDate = updateDate
	}

	static func parse(dictionary: [String: AnyObject]) -> CSPullRequest? {
		let pullRequest = CSPullRequest()
		
		fillWithDictionary(&pullRequest.Title, key: kTitle, dictionary: dictionary)
		fillWithDictionary(&pullRequest.Body, key: kBody, dictionary: dictionary)
		fillWithDictionary(&pullRequest.CreateDate, key: kCreateDate, dictionary: dictionary)
		fillWithDictionary(&pullRequest.UpdateDate, key: kUpdateDate, dictionary: dictionary)

		guard
			let userDictionary = dictionary[kUser] as? [String: AnyObject]
		else {
				return nil
		}
		let user = CSUser.parse(dictionary: userDictionary)
		pullRequest.User = user
		
		return pullRequest
	}
}
