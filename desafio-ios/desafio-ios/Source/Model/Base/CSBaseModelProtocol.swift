//
//  CSBaseModelProtocol.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import Foundation

protocol CSBaseModelProtocol {
	static func fillWithDictionary<T>(_ variable: inout T, key: String, dictionary: [String : AnyObject])
}

extension CSBaseModelProtocol {
	
	static func fillWithDictionary<T>(_ variable: inout T, key: String, dictionary: [String : AnyObject]) {
		if let tempField = dictionary[key] as? T {
			variable = tempField
		}
	}
	
	static func fillObjectWithDictionary<T>(_ variable: inout T, key: String, dictionary: [String : AnyObject]) {
		if let tempField = dictionary[key] as? T {
			variable = tempField
		}
	}
}
