//
//  CSUser.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSUser: NSObject, CSBaseModelProtocol {
	
	// MARK: - Constants
	
	fileprivate static let kHTMLURL = "html_url"
	fileprivate static let kLogin = "login"
	fileprivate static let kAvatarURL = "avatar_url"

	// MARK: - Properties

	var Login: String?
	var HTMLURL: String?
	var AvatarURL: String?
	
	// MARK: - Initializers
	
	override init() { }
	
	init(login: String?, htmlURL: String?, avatarURL: String?) {
		self.Login = login
		self.HTMLURL = htmlURL
		self.AvatarURL = avatarURL
	}
	
	// MARK: - Parser
	
	static func parse(dictionary: [String: AnyObject]) -> CSUser? {
		let user = CSUser()
		
		fillWithDictionary(&user.Login, key: kLogin, dictionary: dictionary)
		fillWithDictionary(&user.HTMLURL, key: kHTMLURL, dictionary: dictionary)
		fillWithDictionary(&user.AvatarURL, key: kAvatarURL, dictionary: dictionary)
		
		return user
	}
}
