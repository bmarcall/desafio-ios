//
//  CSOwner.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSOwner: NSObject, CSBaseModelProtocol {
	// MARK: - Constants
	
	fileprivate static let kLogin = "login"
	fileprivate static let kAvatarURL = "avatar_url"
	
	// MARK: - Properties
	
	var login: String?
	var avatarURL: String?
	
	override init() { }
	
	init(login: String?, avatarURL: String?) {
		self.login = login
		self.avatarURL = avatarURL
	}
	
	static func parse(dictionary: [String: AnyObject]) -> CSOwner? {
		let owner = CSOwner()
		
		fillWithDictionary(&owner.login, key: kLogin, dictionary: dictionary)
		fillWithDictionary(&owner.avatarURL, key: kAvatarURL, dictionary: dictionary)
		
		return owner
	}
}
