//
//  ITHttpStatusCode.swift
//  DesafioTW
//
//  Created by Marçal on 07/01/18.
//  Copyright © 2018 tw. All rights reserved.
//

import UIKit

enum CSHttpStatusCode: Int {
	case Ok = 200
	case Created = 201
	case Accepted = 202
	case PartialContent = 206
	case BadRequest = 400
	case Unauthorized = 401
	case Forbidden = 403
	case NotFound = 404
	case UnprocessableEntity = 422
	case InternalServerError = 500
	case NotImplemented = 501
	case BadGateWay = 502
	case ServiceUnavailable = 503
	case ErrorProcessing = 850
	case Unknown = 999
}

