//
//  CSRepositoryApiProvider.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSRepositoryApiProvider: CSApiProvider, CSRepositoryApiProtocol {
	
	// MARK: - Properties
	
	fileprivate let kRepositoryURL = "/search/repositories?q=language:{language}&sort={sort}&page={pageCount}"
	fileprivate let kLanguageTag = "{language}"
	fileprivate let kSortTag = "{sort}"
	fileprivate let kPageCountTag = "{pageCount}"

	func getRepository(language: String, sort: String, pageCount: String, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback) {
		let dinamicURL = kRepositoryURL.replacingOccurrences(of: kLanguageTag, with: language).replacingOccurrences(of: kSortTag, with: sort).replacingOccurrences(of: kPageCountTag, with: pageCount)
		CSApiProvider.getWith(urlExtension: dinamicURL, successBlock: { (data) in
			successBlock(data)
		}) { (error) in
			failureBlock(error)
		}
	}
}
