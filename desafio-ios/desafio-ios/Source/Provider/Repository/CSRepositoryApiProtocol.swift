//
//  CSRepositoryProtocol.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

protocol CSRepositoryApiProtocol {
	func getRepository(language: String, sort: String, pageCount: String, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback)
}
