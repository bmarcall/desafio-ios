//
//  CSApiProtocol.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import Alamofire

typealias CSSuccessCallback = (_ data: AnyObject) -> Swift.Void
typealias CSFailureCallback = (_ error: Error) -> Swift.Void

internal class CSApiProvider {
	
	// MARK: - Properties
	
	fileprivate static let kIApiProviderUrl = "https://api.github.com"
	
	// MARK: - Initializers
	
	init() { }
	
	// MARK: - Static Methods
	static func getWith(urlExtension: String, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback) {
		Alamofire.request(kIApiProviderUrl + urlExtension).responseJSON { (data) in
			let result = data.result
			switch (result) {
			case .success(let data):
				successBlock(data as AnyObject)
				break
			case .failure(let error):
				failureBlock(error)
				break
			}
		}
	}
	
	// MARK: - Static Methods
	static func postWith(urlExtension: String, params: Dictionary<String, Any>, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback) {
		Alamofire.request(kIApiProviderUrl + urlExtension, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (data) in
			let result = data.result
			switch (result) {
			case .success(let data):
				successBlock(data as AnyObject)
				break
			case .failure(let error):
				failureBlock(error)
				break
			}
		}
	}
}

