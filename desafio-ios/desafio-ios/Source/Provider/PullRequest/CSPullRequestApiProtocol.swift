//
//  CSPullRequestApiProtocol.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

protocol CSPullRequestApiProtocol {
	func getPullRequest(login: String, repositoryName:String, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback)
}

