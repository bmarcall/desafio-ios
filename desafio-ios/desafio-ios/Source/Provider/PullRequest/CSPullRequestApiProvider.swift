//
//  CSPullRequestApiProvider.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSPullRequestApiProvider: CSApiProvider, CSPullRequestApiProtocol {
	
	// MARK: - Constants
	fileprivate let kPullRequestURL = "/repos/{login}/{repositoryName}/pulls"
	fileprivate let kLoginTag = "{login}"
	fileprivate let kRepositoryNameTag = "{repositoryName}"

	// MARK: - Methods
	
	func getPullRequest(login: String, repositoryName: String, successBlock: @escaping CSSuccessCallback, failureBlock: @escaping CSFailureCallback) {
		let dinamicURL = kPullRequestURL.replacingOccurrences(of: kLoginTag, with: login).replacingOccurrences(of: kRepositoryNameTag, with: repositoryName)
		CSApiProvider.getWith(urlExtension: dinamicURL, successBlock: { (data) in
			successBlock(data)
		}) { (error) in
			failureBlock(error)
		}
	}
}

