//
//  CSRepositoryManager.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSRepositoryManager: CSBaseManager {
	
	// MARK: - Properties
	
	private let business = CSRepositoryBusiness()
	
	// MARK: - Methods
	
	func getRepository(language: String, sort: String, pageCount: String, successBlock: @escaping CSSuccessRepositoryDataCallback, failureBlock: @escaping CSFailureCallback) {
		self.business.getRepository(language: language, sort: sort, pageCount: pageCount, successBlock: successBlock, failureBlock: failureBlock)
	}
}
