//
//  CSPullRequestManager.swift
//  desafio-ios
//
//  Created by Marçal on 12/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class CSPullRequestManager: CSBaseManager {
	
	// MARK: - Properties
	
	private let business = CSPullRequestBusiness()
	
	// MARK: - Methods

	func getPullRequest(login: String, repositoryName: String, successBlock: @escaping CSSuccessPullRequestDataCallback, failureBlock: @escaping CSFailureCallback) {
		self.business.getPullRequest(login: login, repositoryName: repositoryName, successBlock: successBlock, failureBlock: failureBlock)
	}
}

