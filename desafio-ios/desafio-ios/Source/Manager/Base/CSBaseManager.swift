//
//  CSBaseManager.swift
//  desafio-ios
//
//  Created by Marçal on 10/01/18.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

typealias CSUICallback = (_ success: Bool, _ error: Error?) -> Void

class CSBaseManager {
	
	lazy var operations: OperationQueue = {
		return OperationQueue()
	}()
	
	// MARK: - Initializers
	convenience init(maxConcurrentOperationCount: Int) {
		
		self.init()
		operations.maxConcurrentOperationCount = maxConcurrentOperationCount
	}
	
	// MARK: - Deinitalizers
	
	deinit {
		operations.cancelAllOperations()
	}
}

